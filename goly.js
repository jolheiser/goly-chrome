class Link {
    constructor(short, url, uses, expiry, last_used) {
        this.short = short;
        this.url = url;
        this.uses = uses;
        this.expiry = expiry;
        this.last_used = last_used;
    }
}

class Status {
    constructor(count, version) {
        this.count = count;
        this.version = version;
    }
}

class Client {
    constructor(baseURL) {
        this.baseURL = baseURL;
    }

    async getStatus() {
        if (this.baseURL === '') throw 'Goly base URL cannot be blank.\nConfigure a valid Goly base URL.';
        const resp = await fetch(`${this.baseURL}/api/status`).catch((err) => {
            throw `Couldn't connect to ${this.baseURL}`;
        });
        const json = await resp.json();
        if (!resp.ok) {
            throw JSON.stringify(json);
        }
        return Object.assign(new Status(), json);
    }

    async getLink(short) {
        if (this.baseURL === '') throw 'Goly base URL cannot be blank.\nConfigure a valid Goly base URL.';
        const resp = await fetch(`${this.baseURL}/api/link/${short}`);
        const json = await resp.json();
        if (json.status !== 200) {
            throw JSON.stringify(json);
        }
        return Object.assign(new Link(), json.link);
    }

    async getQR(short) {
        if (this.baseURL === '') throw 'Goly base URL cannot be blank. Configure a valid Goly base URL.';
        const resp = await fetch(`${this.baseURL}/api/link/${short}/qr`);
        if (!resp.ok) {
            throw JSON.stringify(await resp.json());
        }
        return await resp.blob();
    }

    async setLink(link) {
        if (this.baseURL === '') throw 'Goly base URL cannot be blank.\nConfigure a valid Goly base URL.';
        const resp = await fetch(`${this.baseURL}/api/link`, {
            method: 'POST',
            body: JSON.stringify(link),
        });
        const json = await resp.json();
        if (json.status !== 201) {
            throw JSON.stringify(json);
        }
        return Object.assign(new Link(), json.link);
    }
}
