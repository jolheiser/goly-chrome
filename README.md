# Goly Chrome

A Chrome extension for [Goly](https://gitea.com/jolheiser/goly).


## Screenshots

![context](images/context.png)

![toolbar](images/toolbar.png)