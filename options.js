const baseUrl = document.getElementById('base-url');
const saveButton = document.getElementById('save');

chrome.storage.sync.get(['golyBaseUrl'], (result) => {
    baseUrl.value = result.golyBaseUrl || '';
});

saveButton.addEventListener('click', () => {
    const url = baseUrl.value;
    chrome.storage.sync.set({
        golyBaseUrl: url
    }, () => {
        chrome.runtime.sendMessage({url: url});
    });
});
